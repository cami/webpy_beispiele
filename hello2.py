import web

urls = (
        '/', 'index'
        )

# using a template in the directory templatess
render = web.template.render('templates/')

class index:
    def GET(self):
        hello = 'Hello World!'
        return render.index(hello)

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
