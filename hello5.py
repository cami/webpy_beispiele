import web

# everything after the slash is an own parameter string and will be printed
urls = (
        '/form', 'form',
        '/(.*)', 'index'
        )

# using a template in the directory templatess and define some base template
render = web.template.render('templates/', base="base")

class index:
    def GET(self, hello):
        return render.index(hello)

class form:

    valid = web.form.regexp(r"^[0-9]+$", '--> must be numeric!')

    formular=web.form.Form(
            web.form.Textbox("hello", valid, description="input: "),
            web.form.Button("send ...")
            )

    def GET(self):
        return render.form(self.formular)

    def POST(self):
        if not self.formular.validates():
            return render.form(self.formular)
        else:
            # `d.` needs to be entered
            return render.index(self.formular.d.hello)


if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
