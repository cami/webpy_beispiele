# Web.py - Web-Anwendungen in Python

Das nachfolgende Beispiel stammt ursprünglich aus einem Vortrag der FrOSCon 2022, welcher sich damit beschäftigt. Der Vortrag kann [hier](https://media.ccc.de/v/froscon2022-2747-web_py_web-anwendungen_in_python) angeschaut werden.

## Development

- clone the repository: `git clone`
- go into the cloned directory: `cd `
- create a virtual environment for python and use it: `python -m venv venv && source venv/bin/activate`
- install the dependencies: `pip install -r requirements.txt`

## Explanation of examples

- `hello1.py` is the most simple example. It will only return the given string.
- `hello2.py` is a little bit more advanced and will render the variables in a given template (i.e. index.html)
- `hello3.py` will also use a base template (for headers footers and so on) and then use the index template
- `hello4.py`
- `hello5.py`
