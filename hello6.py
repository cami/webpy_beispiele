import web

urls = (
        '/', 'database'
        )

# if another database is needed you can only change the db type
db = web.database(dbn='sqlite', db='testdb.sql')

render = web.template.render('templates/', base="base")

class database:
    def GET(self):
        # person is the table (select is default)
        # there's the possibility to use `.query` for specific things
        fn = db.select("persons")
        return render.database(fn)

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
