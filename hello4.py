import web

# everything after the slash is an own parameter string and will be printed
# - nothing after slash: "... no input"
# - something after slash => The given word after the slash
urls = (
        '/(.*)', 'index'
        )

# using a template in the directory templatess and define some base template
render = web.template.render('templates/', base="base")

class index:
    def GET(self, hello):
        return render.index(hello)

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
