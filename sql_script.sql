CREATE TABLE persons(
    first   text NOT NULL
    , name    text NOT NULL
);
INSERT INTO persons VALUES("Fritz", "Heiri");
INSERT INTO persons VALUES("Sepp", "Irgendwas");
INSERT INTO persons VALUES("Annalena", "Franz");
